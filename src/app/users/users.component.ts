import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'app-users',
  templateUrl:  './users.component.html',
     styles: [`
+        .users li { cursor: default; }
+        .users li:hover { background: #ecf0f1; } 
+        .list-group-item.active, 
+        .list-group-item.active:hover, 
+        .list-group-item.active:focus { 
+            background-color: #ecf0f1;
+            border-color: #ecf0f1; 
+            color: #2c3e50;
+        }
+    `]
})
export class UsersComponent implements OnInit {

    users;

   currentUser;

   select(user){
     this.currentUser = user;
   }

 


  constructor(private _usersService:UsersService) { }

  ngOnInit() {
    this.users = this._usersService.getUsers();
  }

}
