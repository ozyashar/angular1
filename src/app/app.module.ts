import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import {UsersService} from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    UserComponent,
    PostsComponent,
    PostsService
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  //providers: [],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
